#!/bin/bash
set -e

# Gitbook is deprecated and no longer works in default CC7 NodeJS environment.
# Special care must be taken to install gitbook v3.2.3 + graceful-fs v4.2.0

npm install gitbook-cli gitbook-plugin-anchorjs

pushd node_modules/npm/
npm install graceful-fs@4.2.0
popd

./node_modules/gitbook-cli/bin/gitbook.js install 3.2.3
