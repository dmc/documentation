# Installing via Pip

As mentioned before, `gfal2-python` is a C++ library offering a thin layer of
Python wrappers via `PyBoost`. We recommend installing it via RPMs, 
but it is also available on [Pypi/gfal2-python][1].

Installing it via `pip install gfal2-python` will reproduce the compilation
process, hence why you need to make sure you have all the right dependencies.
This guide details the steps needed to get all the dependencies, starting
from a *blank* machine.

This guide assumes you already the [DMC repositories](../repositories.md) installed. 
We recommend installing from the `DMC production` for the latest RPMs. In this example,
the build platform is `CC7` and we will use `v1.11.0`, but the process applies
the same for other versions or build platforms.

```bash
# Clone the gfal2-python repository with the desired version (example for v1.11.0)
$ git clone --branch v1.11.0 https://github.com/cern-fts/gfal2-python.git
$ cd gfal2-python/

# Install standard set of build packages
$ ./ci/fedora-packages.sh

# Build the source RPM and install dependencies
# Available build flags:
#   All: --without python3 
#   CC7: --without docs / --without python2
$ cd packaging/
$ RPMBUILD_SRC_EXTRA_FLAGS="--without docs --without python2" make srpm
$ yum-builddep -y gfal2-python-1.11.0-1.el7.cern.src.rpm

# See work-around for CC7 (no python-devel package)
$ pip install gfal2-python
```

Enjoy using the Gfal2 Python bindings!

#### CC7 work-around (no python-devel package installed)

On CC7 platform, when no `python-devel` is installed, `pip install` will fail
in the compiling phase with the following error:
```
/usr/include/boost/python/detail/wrap_python.hpp:50:23: fatal error: pyconfig.h: No such file or directory
 # include <pyconfig.h>
```

To work around it, we must manually add the `pyconfig.h` 
directory to the C++ linker path:
```bash
$ export PYCONFIG_INCLUDE_DIR=$(find /usr/include -name "pyconfig.h" -printf "%h\n")
$ export CPLUS_INCLUDE_PATH="$CPLUS_INCLUDE_PATH:$PYCONFIG_INCLUDE_DIR"
$ pip install gfal2-python 
```

In case the `python-devel` package is installed, 
then this work-around is not needed.

[1]: https://pypi.org/project/gfal2-python/
