# SRM-IFCE
srm-ifce is a client side implementation of the [SRMv2 specification](https://sdm.lbl.gov/srm-wg/doc/SRM.v2.2.html)
for GFAL and FTS. SRM means Storage Resource Manager Interface, it is a
specification of a SOAP interface providing a generic way to manage
distributed storage systems.
