# Data Management Clients

The "Data Management Clients" are a family of libraries and tools
that implements, and abstract away, the details of Grid and non-Grid
protocols.

You can query which version is in which stage of release with our
[release matrix](http://dmc-docs.web.cern.ch/dmc-docs/versions/index.html).

For more details on each protocol, you can consult the
[protocol cheat-sheet](protocol-cheat-sheet.md) to see how do they compare.

## Support
For support, you may contact us via [dmc-support@cern.ch](mailto:dmc-support@cern.ch),
or via [Service Now](https://cern.service-now.com/service-portal/) (note that you need
an account at CERN).
