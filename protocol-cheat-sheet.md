# Protocol Cheat Sheet
This is a quick reference of the different protocols supported by GFAL2.

| Protocol | Auth | Data Transfer | Space Tokens\* | Third Party Copy | Staging/QoS | Notes |
|----------|------|---------------|----------------|------------------|---------|-------|
| lfc  | X509 | <span style="color:red">NO</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | Deprecated. Maintained by DPM team. |
| [srm](https://sdm.lbl.gov/srm-wg/doc/SRM.v2.2.html) | X509 | <span style="color:red">NO</span> | <span style="color:green">YES</span> | <span style="color:red">NO</span> | <span style="color:green">YES</span> | XML-Based. Some calls are asynchronous (token-based) |
| gsiftp | X509 | <span style="color:green">YES</span> | <span style="color:red">NO</span> | <span style="color:green">YES</span> | <span style="color:red">NO</span> | Async API (callbacks). Maintained by Globus, to be adopted by OSG. |
| [xrootd](http://xrootd.org/) | X509, Kerberos | <span style="color:green">YES</span> | <span style="color:orange">As part of the URL</span> | <span style="color:green">YES</span> | <span style="color:orange">YES, EOS/CTA</span> | Async and sync API (Posix-like). |
| http/dav | X509, Tokens | <span style="color:green">YES</span> | <span style="color:orange">As part of the URL</span> | <span style="color:green">YES</span> | <span style="color:orange">YES, CDMI-QoS</span> | DAV is XML-Based. Copies *may* fallback to streaming if neither endpoint support copies. |
| s3   | X509, Tokens, AWS Signature | <span style="color:green">YES</span> | <span style="color:orange">As part of the URL</span> | <span style="color:orange">Only as passive endpoint</span> | <span style="color:red">NO</span> | Implemented by HTTP-plugin via [Davix](https://github.com/cern-fts/davix). |
| rfio | X509, by hostname | <span style="color:green">YES</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | Deprecated. Maintained by DPM. |
| dcap | X509 | <span style="color:green">YES</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | Deprecated. dCache-specific. |
| [dropbox](https://github.com/cern-it-sdc-id/gfal2-dropbox) | OAuth2 |  <span style="color:green">YES</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | JSON-Based. |
| [sftp](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol) | User/password or SSH private key | <span style="color:green">YES</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | <span style="color:red">NO</span> | Experimental |

## Transfers / copies
While namespace operations are relatively straight forward - just look at the
URI and choose depending on the scheme -, transfers can get trickier, since they
may involve different combinations of two protocols.

It is up to the plugins to decide whether they take over any transfer involving
them or not. The following table summarizes what happens in each case.

| Source | Destination | Plugin | Explanation |
|--------|-------------|--------|-------------|
| srm    | \* | srm plugin | srm needs to resolve the transfer URL before doing the copy. Also, it handles space tokens. |
| \* | srm | srm plugin | Same. |
| gsiftp | gsiftp | gsiftp plugin | Third party copies. |
| xrootd | file   | xrootd plugin | More performant API call than read-write calls. |
| file | xrootd | xrootd plugin | Same. |
| xrootd | xrootd | xrootd plugin | Third party copies. |
| file | http | http plugin | One single PUT instead of chunk writes. |
| http | http | http plugin | Third party copies w/o fallback (push, pull, streamed). |

For any other combinations, gfal2 core takes over, and does the transfer via
`open`, `read`, `write`, `close` calls.

## What is a space token?
A space reservation. Can be specified for the source and for the destination.
The space reservation can be disk, and/or tape.

![space tokens](spacetokens.png "Space tokens")

It is convenient to make a difference between two terms:

* **Space token**: An opaque ID, internal to the storage. It can be from an integer (dCache) to a UUID (DPM).
* **Space token description**: A human-readable label assigned to the space token. I.e. "ATLASSCRATCHDISK".

Normally users use "space token description".
