# Glossary
- **CA:** Certificate Authority
- **CTA:** CERN Tape Archive - future tape backend for EOS
- **DAV:** Distributed Authoring and Versioning (also, WebDAV)
- **dCache:** Storage Element
- **DCAP:** dCache access protocol client library
- **DPM:** Storage Element (Disk Pool Manager)
- **EOS:** Storage Element built on top of the XRootD framework
- **EPEL:** Extra Packages for Enterprise Linux
- **HTTP:** Hypertext Transfer Protocol
- **GFAL2:** Grid File Abstraction Layer 2
- **GSIFTP:** Extension of the File Transfer Protocol
- **GridFTP:** Extension of the File Transfer Protocol
- **LHC:** Large Hadron Collider
- **LFC:** LCG File Catalog
- **OSG:** Open Science Grid
- **Proxy:** A X509 certificate signed by another proxy or by the user End Entity Certificate. (As opposed by a "regular" X509 certificate, which is signed by a Certificate authority)
- **RFIO:** Remote File I/O
- **RHEL:** RedHat Enterprise Linux
- **S3:** Amazon Simple Storage service
- **SRM:** Storage Resource Manager
- **STORM:** Storage Element (STOrage Resource Manager)
- **VO:** Virtual Organization
- **VOMS:** Virtual Organization Membership System
- **WebDAV:** Distributed Authoring and Versioning (also, DAV)
- **X509:** Standard that defines the format of public key certificates.
- **XRootD:** XRootD framework for data repositories, and, by inclusion, its native
protocol.

