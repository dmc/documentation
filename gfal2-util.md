# GFAL2-Util
GFAL2-Util are a group of command line tools for file manipulations with
any protocol managed by gfal2.

## Commands
We list here the list of available commands. For more details about each one
of them, run them with the `--help` flag, or check its `man` page.

##### `gfal-cat`
Dump the content of a file into standard output

##### `gfal-chmod`
Change the permission bits of a file\*

##### `gfal-copy`
Copy a source into a destination. Can chain intermediate destinations.
The content may be streamed via the host if the protocol does not support third
party copies.

##### `gfal-ls`
List the content of a directory

##### `gfal-mkdir`
Create a directory

##### `gfal-rename`
Rename a file or directory

##### `gfal-rm`
Delete a file or directory

##### `gfal-save`
Writes into a file what is passed via standard input

##### `gfal-stat`
Stats a file or directory. Similar to `stat`

##### `gfal-sum`
Retrieves the checksum of a file\*

##### `gfal-xattr`
Get all, get or set an extended attribute of a file or directory\*.
Each protocol support a different set of attributes, but some interesting ones are:

* `user.replicas` Obtain a TURL from an SRM url, or a list of replicas from an LFC one.
* `user.status` Obtain if a file is ONLINE, NEARLINE, ONLINE_AND_NEARLINE, or other. Relevant for tape systems, via SRM only.
* `user.chksumtype` Checksum type associated to a file.
* `user.checksum` Checksum value associated to a file.

(\*) Not supported by all protocols


## Legacy commands
Some commands are provided for legacy purposes. The three that have something
to do with replicas, can be executed via extended attributes.

##### `gfal-legacy-bringonline`
Request the staging of a file into disk from tape. Supported by SRM and,
experimentally, by XRootD.

##### `gfal-legacy-register`
Add a new replica to a file in a catalog. This is equivalent to

```bash
gfal-xattr "lfn://<path>" "user.replicas=+srm://<new-replica>"
```

The plugin may do some validation of the new replica: file size, checksum...

##### `gfal-legacy-replicas`
List replicas for a file in a catalog. This is equivalent to

```bash
gfal-xattr "lfn://<path>" "user.replicas"
```

##### `gfal-legacy-unregister`
Remove a replica for a file in a catalog. This is equivalent to

```bash
gfal-xattr "lfn://<path>" "user.replicas=-srm://<replica-to-remove>"
```

The plugin may remove the entry in the catalog when there are no more replicas
left.
