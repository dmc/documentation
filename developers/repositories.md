# repositories
We use git for the version control. Previously,
[Subversion](https://svnweb.cern.ch/trac/lcgutil/browser) was used.

A basic knowledge of git is obviously required. There are plenty of
[tutorials](https://www.google.ch/search?q=git+tutorial) out there.

All of the DMC code is stored at [CERN's GitLab](https://gitlab.cern.ch/dmc), under
the same group.

## Branches
Most DMC repositories follow the following branching model.

### develop
Integrated development version. Should be in a reasonably consistent state,
but not necessarily stable.
This means, new features go here, and they should be in a workable state.

At the very least it must compile, and the packaging, if any, must work.

### master
__Stable branch__. Only critical bugfixes. Runs on the FTS3 pilot service first, and then
in the FTS3 production services once it has been validated enough.

### Other branches
develop should be consistent and workable. For big change sets, you can keep them in a
separate branch, and merge them into develop when they are ready (remember to rebase first).
You don't need to keep these branches in the remote repositories.

## Keep history clean and tidy
  1. Write meaningful commit messages
  2. Specify any relevant JIRA ticket in the commit
  3. Avoid multipurpose jumbo commits
    * They should not contain unrelated changes
      * two bug fixes in one commit, bad
      * new feature + reformatting, very bad
  4. Before pushing, group related commits (yes, [you can do this with git](http://stackoverflow.com/questions/6884022/collapsing-a-group-of-commits-into-one-on-git))
    * If you have two commits for one single bug fix, try grouping them whenever it makes sense
  5. Do not force push!
    * Seriously, do not force push
    * Acceptable for feature or bug branches that are not shared,
      very forbidden for shared branches, verily

## Use pull requests
For minor changes, it can be skipped, but for big change sets, do merges via
pull requests, and send a mail nicely asking for a code review.
