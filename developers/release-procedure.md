# Release Procedure

This page strives to provide a step-by-step guide on releasing a new DMC package.
For larger considerations on the Release Process, please consult the [Release Guidelines](release-guidelines.md).

The build process is not covered in this section. Although with the migration to `gitlab-CI`,
the procedure is now very similar among the different DMC projects. For complete 
details, please consult the `.gitlab-ci.yml` file of the project repository.

1. Make sure all relevant [DMC JIRA][1] issues are `Closed` and the wanted `Fix version` is set
2. Make sure the `packaging/rpm/<name>.spec` file includes the correct package dependency versions
3. Increase the release number:
   - In the spec file
   - In the main `CMakeLists.txt` file
4. Add author and description entries in the spec file changelog
5. Add author and description entries in the `packaging/debian/changelog` file
6. Fill in the release notes (generally `RELEASE-NOTES` files):
   - Use the [JIRA Releases][2] notes: JIRA > Releases > *Project* > Release Notes
   - Copy the text message, apply formatting and add it to the release notes file
7. Commit these changes on `develop` branch, preferably as one commit. E.g.: `Prepare release <version>`
8. Make sure the build/pipeline succeeds
9. Change to `master` branch and merge `develop`:
   - `git merge "develop" --message 'Merge branch "develop" for release <version>'`
   - Ideally, there should be no merge conflict. Otherwise, the development process went wrong somewhere
10. Tag the master branch with the new version: `git tag <version>`
    - Most projects have the following version format: `v#.##.#`. Example: `v2.20.0`
    - Davix uses the following version format: `R_#_#_#`. Example: `R_0_8_0`
    - **Note**: Davix requires tags to be annotated. Example: `git tag --annotate --message "Tag for version 0.8.0" R_0_8_0`
11. Push the `master` branch and new tag to remote:
    - `git push --atomic origin master <tag>`
12. Once the pipeline succeeds, the release candidate RPMs should now be uploaded to [RC repository][3]
13. Mark the JIRA version as `Released` in the [JIRA Releases][2] page
14. *(Davix only)*: Publish a new release on the Davix GitHub [Releases page][4]
    - Use the same release notes as from step 6
    - The *blessed* tarball is the one generated by the `packaging/make-dist.sh` script,
      which is also the same one packaged into the source RPM
15. Install the new release on FTS3-Pilot:
    - `wassh --cluster fts/pilot/live --user root "yum update -y"`
16. If happy with the release, move it to production repository:
    - Each tag pipeline has a publish job to move the latest packages from RC to production repository
17. Announce the release!


[1]: https://its.cern.ch/jira/projects/DMC/issues
[2]: https://its.cern.ch/jira/projects/DMC
[3]: https://dmc-repo.web.cern.ch/dmc-repo/rc
[4]: https://github.com/cern-fts/davix/releases
