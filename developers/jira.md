# JIRA
We use the IT Jira instance to keep track of our tickets.
For DMC, the relevant project is, obviously, [DMC](https://its.cern.ch/jira/browse/DMC),
but given the extremely close relationship with FTS3, the [FTS3](https://its.cern.ch/jira/browse/FTS3)
tracker can be of interest too.

Even though having a ticket, or set of tickets, bound to changes in the codebase
is good practice, we leave it to the good criteria of the developers to do this.
There is no enforcing of ticket numbers on each commit or anything like that, since
that may be too extreme (requiring a ticket for fixing a typo on a comment would be too much,
wouldn't it?)

Still, major changes, new functionalities and bug fixes *must* have a corresponding ticket,
mostly for tracking and compiling release notes.

Tickets are very useful also to try and track back the *reason* for a given change.
Seeing a diff tells you what changed, but not necessarily why!

Fill the description and comments properly too. Going to a ticket just to see the same
you could see in the commit (i.e, no rationale behind) isn't good.
