# Developers
This chapter of the documentation is targeted at DMC developers and contributors.

Of course, you are welcome to go through it even if you do not plan to commit
any code!
