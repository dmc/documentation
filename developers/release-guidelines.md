# Release Guidelines

Releasing a component, or set of components, is indeed not a trivial task!
I will try to summarize here the required steps to make a shiny new release
for any of the DMC components.

Have a look at the [component summary](../components.md) to know which are they.

For a step-by-step guide on how to do the release, please consult the
[release procedure](release-procedure.md) document.

## Coexisting versions

We can consider that there can be up to four versions coexisting: production
(epel), pre-production (epel-testing), release candidate and development.

Each one has different stability assumptions, so it is worth knowing where
changes should be made, when required.

|                        | EPEL | EPEL-TESTING | RELEASE-CANDIDATE | DEVELOPMENT |
|------------------------|------|--------------|-------------------|-------------|
| **New features**           | <span style="color: red">NO</span> | <span style="color: red">NO</span> | <span style="color: red">NO</span> | <span style="color: green">YES</span>
| **Improvements**           | <span style="color: red">NO</span> | <span style="color: red">NO</span> | <span style="color: orange">NORMALLY NOT</span> | <span style="color: green">YES</span> |
| **Non critical bug-fixes** | <span style="color: red">NO</span> | <span style="color: red">NO</span> | <span style="color: green">YES</span> | <span style="color: green">YES</span> |
| **Critical bug-fixes**     | <span style="color: red">NO</span> | <span style="color: green">YES</span> | <span style="color: green">YES</span> | <span style="color: green">YES</span> |

Releases should go from development to release candidate, and left there for at
least one or two weeks to validate it doesn't functionally break anything.

For some components, as `gfal2`, `cgsi-gsoap` and `srm-ifce`, this version will
be installed on the `FTS3-PILOT` service, so if it doesn't break here, that's a
good sign.

For all of them, development versions will be installed in `dmc-ui-devel`,
and release-candidate in `dmc-ui-rc`.

Now, for some description of the typical tasks belonging to each stage.

### Development (develop branches)

Except minor changes - as typo fixes in commentaries - pretty much every change
must have a corresponding ticket in JIRA, assigned to the right component and
version. This is not enforced (via hooks or similar), but it is good practice.

Every time that a production release is done, the minor version should be
increased, and the revision reset back to 0 (i.e. `2.5.8` => `2.6.0`)

Changes will be immediately picked and built by [Jenkins](https://jenkins-fts-dmc.web.cern.ch/),
and the resulting rpms (versioned as $version-r$build) will be copied to the
unstable repositories. Normally, after a short period, the development machines
(`dmc-ui-devel` and `fts3-devel`) will upgrade via Puppet.

Finally, every night a test suite will be executed against the development
version of the components. Testing gfal2 implicitly tests `srm-ifce` and
`cgsi-gsoap`. `gfal2-util` has a small test suite that runs in each build.

Even though it is acceptable to have development broken for a couple of days,
try fixing, or identifying, the errors before doing anything else. Otherwise
it becomes a pain later to recover normality. Been there, done that. Not good.

### Release Candidate (master branches)
No new features should be added to a release candidate. Bug-fixes, of course,
that's what the release candidate is for!

Same as before, the changes will be immediately picked and build by Jenkins
and copied to the RC repositories, but there is no automatic version increase.
That's up for the developer to trigger an upgrade increasing the revision
or release number (i.e. `2.5.8` => `2.5.9`).

**Note:** The scripts that copy the rpms to the repository will check there
isn't an existing rpm with the same version. If there is, the build will fail.

The release candidate machines (`dmc-ui-rc`, `fts3-pilot`) will upgrade
automatically thanks to Puppet.

**Note:** For convenience, it is acceptable to do incremental merges
(i.e, merge `develop` into `master`, bump version from `2.5.7` to `2.5.8`),
so we trigger the Pilot to upgrade, but keep that to a minimum!

**Note:** Remember to apply eventual fixes to development as well, or,
if you wish, fix in `develop` and backport to `master`.

### From Release Candidate to EPEL

* Mark the corresponding version in JIRA as closed
* Review the tickets, and rephrase the titles if needed so their subjects are clearer
* Try prefixing the tickets with the affected plugin name and/or Core in the case of gfal2
* Update the RELEASE-NOTES in `master`, with the proper release dates, if not already done before the merge
* Update the RELEASE-NOTES in `develop` so they are in sync
* Tag the new version
* Create a new release in Drupal

Now it is ready to go into EPEL

### EPEL-TESTING

I am not talking much about this stage, as this is the realm of [Fedora](http://fedoraproject.org/wiki/Packaging:Guidelines)
and [EPEL Guidelines](https://fedoraproject.org/wiki/EPEL/GuidelinesAndPolicies).

You need to be a packager at this stage, so I assume you already now how to proceed
(update spec file, upload new sources, etc...)

Keep in mind the dependency graph! If there are API changes, you will need to
respect the order (`cgsi-gsoap` first, then `srm-ifce`, then `davix`, then `gfal2`, then
`gfal2-python` and `gfalFS`, and finally `gfal2-util`).

So, for instance, if a new API call was introduced in `srm-ifce`, you will need
to build that first, make a [buildroot override](https://fedoraproject.org/wiki/Bodhi/BuildRootOverrides),
and then build `gfal2`.

Packages in this stage will be upgrade by some testing machines out there.
Keep an eye and make sure there are no broken dependencies.

### EPEL

Finally, Bodhi will let you know when can you push the built rpms.
Keep in mind again the dependency order, and push `srm-ifce` before `gfal2`,
and so on...

#### When to modify directly in EPEL

Never :)

Seriously, if being already in epel or epel-testing you find, or someone
reports, a very bad bug that needs immediate action, you can either create a
new version with the patch in the repository, and do a full release,
or you could just apply a patch in Fedora and increase the release number.

To accelerate the release of these urgent patches, you will need to ask other
packagers to give karma to the release.
