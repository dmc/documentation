# Deprecated Plugins

This page contains information about deprecated Gfal2 plugins, 
mostly for historical purposes.

#### gfal2-plugin-dcap
Implements the [dcap](https://github.com/dCache/dcap) protocol. The url
must use the scheme `dcap://` or `gsidcap://`.

I have never seen this plugin used in production, and its
[commit history](https://gitlab.cern.ch/dmc/gfal2/commits/develop/src/plugins/dcap)
proves it has had no maintenance besides adapting to internal gfal2 changes.

#### gfal2-plugin-lfc
With this plugin, gfal2 can operate with the LFC (LCG File Catalog). Note
that this protocol doesn't do data transfer, only namespace operations.

However, the plugin will resolve a replica, and use it for data transfer
if the client application tries to perform data operations.

#### gfal2-plugin-rfio
RFIO plugin is rarely used. As with the dcap one, you can see on the
[commit  history](https://gitlab.cern.ch/dmc/gfal2/commits/develop/src/plugins/rfio)
there is barely any activity on it.
