# GFAL2 FAQ

## Where are the configurations files of GFAL2?
The default directory for gfal2 configuration file is /etc/gfal2.d/.
It can be overriden via the environment variable `GFAL_CONFIG_DIR`.

## How to use a specific transport protocol with SRM?
The priority list for SRM protocol resolution is defined in the configuration
file `srm_plugin.conf` with the parameter `TURL_PROTOCOLS`.

Example: `TURL_PROTOCOLS=root;http;gsiftp;rfio;dcap`
Will use use in order of preference if supported by the server "root -> http -> gsiftp -> rfio -> dcap"

## I need to create a GFAL2 plugin, How to do it?
Install the package gfal2-devel and gfal2-doc.

## What about the license?
GFAL2 is only under Apache 2.0 license.

## What are the environment variables used by GFAL2 core?
* `GFAL_PLUGIN_DIR` Path of the plugin directory (default is `/usr/lib64/gfal2-plugins/`)
* `GFAL_CONFIG_DIR` Path of the configuration file directory (default is `/etc/gfal2.d/`)
* `LCG_GFAL_INFOSYS` URL of the bdii
