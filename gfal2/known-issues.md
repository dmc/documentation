# GFAL2 Known Issues

## [DMC-853](https://its.cern.ch/jira/browse/DMC-853) GridFTP session reuse + PUT / GET through the same connection hangs

### Situation
* GridFTP session reuse is enabled
* The remote endpoint supports the GETPUT extension

### Symptoms
* When uploading and then downloading a file through the same connection, the download process freezes, potentially causing a timeout.

### Cause:
* Globus denies it being a bug on the Globus Toolkit: [Issue 57](https://github.com/globus/globus-toolkit/issues/57)
* Maybe a bug in dCache?

### Workarounds:
Disabling GridFTP session reuse avoids these timeouts.
It can be done programmatically, if you can not modify the configuration files:

```python
ctx.set_opt_boolean("GRIDFTP PLUGIN", "SESSION_REUSE", False)
```
