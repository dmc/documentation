# GFAL2
GFAL (Grid File Access Library ) is a C library providing an abstraction layer
of the grid storage system complexity.
The version 2 of GFAL tries to simplify at the maximum the file operations in
a distributed environment.
the complexity of the grid is hidden from the client side behind a simple
common POSIX API.

GFAL2 is able to manage all the common used file access protocols in a
Grid & cloud environment

* Dcap & GSIDCAP
* LFN & LFC (LFC catalog)
* RFIO,  DPM or CASTOR
* SRM
* GridFTP
* HTTP/WebDav
* XROOTD
* Local files

## 10 good reasons to switch to GFAL2:
* Has a new *Error report system*: protocol independent error code, human
readable string, trace mode and logger system.
* Is simpler and portable, with only one API for all the protocols.
Your application can switch between SRM, GSIFTP, xrootd, Http, Webdav, GSIDCAP,
etc... without any modification.
* Can manage file transfers at high level (gfal2-transfer API, core of FTS3).
* Is extensible with the new plugin system. Adding support for a new protocol
or a new catalog without touching the client application is easy.
* Is now designed to be as POSIX as possible.
* Manages the extended attributes system for advanced grid operations (
get guid, set/get replicas, set/get comments).
* Is delivered with gfalFS , a NEW file system which can mount any compatible
grid URL.
* Is fully supported, without configuration and easier to install.

## What are the main changes from gfal1?
* The NON-POSIX functions of 1.0 do not exist anymore in the POSIX API, they are
mapped to a POSIX one (setxattr/getxattr) or available in the file API (checksum).
* The gfal_request complex system is now replaced by a simple POSIX calls and getxattr/setxattr
* GFAL2 provides the getxattr, setxattr, listxattr calls for the platform's
specific calls (turl resolution, guid resolution, ...)
* The Python API has been reworked, more pythonic and easier to use, follows now
the python philosophy and is able to manage the exceptions properly.
