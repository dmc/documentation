# Repositories

| Platform | OS Stable | OS Testing | Production | Release Candidate | Unstable |
|----------|-----------|------------|------------|-------------------|----------|
| CentOS7/EL7 | [EPEL7](https://dl.fedoraproject.org/pub/epel/7/) | [EPEL7 Testing](https://dl.fedoraproject.org/pub/epel/testing/7/) | [EL7 Prod](https://dmc-repo.web.cern.ch/dmc-repo/dmc-el7.repo) | [EL7 RC](https://dmc-repo.web.cern.ch/dmc-repo/dmc-rc-el7.repo) | [EL7 Devel](https://dmc-repo.web.cern.ch/dmc-repo/dmc-ci-el7.repo) |
| CentOS8/EL8 | [EPEL8](https://dl.fedoraproject.org/pub/epel/8/) | [EPEL8 Testing](https://dl.fedoraproject.org/pub/epel/testing/8/) | [EL8 Prod](https://dmc-repo.web.cern.ch/dmc-repo/dmc-el8.repo) | [EL8 RC](https://dmc-repo.web.cern.ch/dmc-repo/dmc-rc-el8.repo) | [EL8 Devel](https://dmc-repo.web.cern.ch/dmc-repo/dmc-ci-el8.repo) |

##### Poorly maintained

The following platforms are poorly maintained (generally *best-effort* basis):

| Platform | Unstable |
|----------|----------|
| Debian (Testing) | [x86_64](http://grid-deployment.web.cern.ch/grid-deployment/dms/dmc/repos/dmc-debian.list) |
| Ubuntu (16.10 Yakkety Yak)  | [x86_64](http://grid-deployment.web.cern.ch/grid-deployment/dms/dmc/repos/dmc-ubuntu-yakkety.list) |
| Ubuntu (17.04 Zesty Zapus)  | [x86_64](http://grid-deployment.web.cern.ch/grid-deployment/dms/dmc/repos/dmc-ubuntu-zesty.list) |
| MacOS X | [Homebrew Tap](https://github.com/cern-it-sdc-id/homebrew-dmc) |
