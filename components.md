# Components
Here you can see a diagram summarizing the main dependency graph of each
part of the DMC family.

![Components](components.png "DMC Components")

* **Black** DMC libraries and tools. For more information, see the left menu.
* **Dotted gray** FTS is one of the main users of DMC.
* **Blue** Plugins. For building, they depend on gfal2, but gfal2 loads them at runtime.
* **Dotted red** External dependencies, not part of DMC.
    * [*liblfc*](http://lcgdm.web.cern.ch/lfc) Maintained by the LCGDM team.
    * [*Globus Toolkit*](https://github.com/globus/globus-toolkit) To be adopted by OSG. Formerly maintained by the Globus Alliance.
    * [*libXrd*](http://xrootd.org/docs.html) Client libraries from the XRootD framework.
    * [*libcurl*](https://curl.haxx.se/libcurl/) Free Software Library.
    * [*libdpm*](http://lcgdm.web.cern.ch/dpm) Maintained by the LCGDM team.
    * [*libdcap*](https://www.dcache.org/manuals/libdcap.shtml) Maintained by the dCache team.
