# gfalFS
gfalFS is a FUSE module for GFAL2. It is able to mount any endpoint of the
Grid like a local folder.

# How to use it?
Authenticate yourself with the VOMS tools (`voms-proxy-init`) and run

```bash
gfalFS "/your_mount_point" "the_remote_url"
```

## Example
```bash
mkdir "~/mount_tmp"
gfalFS "~/mount_tmp" "davs://dpmhead-rc.cern.ch/dpm/cern.ch/home"
ls "~/mount_tmp/"
cat "~/mount_tmp/dteam/services"
gfalFS_umount "~/mount_tmp"
```
