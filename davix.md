# DaviX
The DaviX project aims to make file management over HTTP-based protocols simple.
The focus is on high-performance remote I/O and data management of large
collections of files. Currently, there is support for the
[WebDav](https://en.wikipedia.org/wiki/WebDAV),
[Amazon S3](https://en.wikipedia.org/wiki/Amazon_S3),
[Microsoft Azure](https://azure.microsoft.com/),
and [HTTP](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol) protocols.

DaviX is multi-platform, [open source](https://github.com/cern-fts/davix)
and written in C++.

## Documentation
[Click here](https://davix.web.cern.ch)
for the documentation of the latest version.

## Description
DaviX is composed of two components:
* **libdavix:** a C++ library. it offers an HTTP API, a remote I/O API and a
POSIX compatibility layer.
* **davix\-***: several utilities for file transfer, large collections of files
management and large files management.

DaviX supports features like session reuse, redirection caching, vector
operations, [Metalink](https://en.wikipedia.org/wiki/Metalink),
X509 client certificate, proxy certificate, SOCKS4/5 or VOMS.

## Installation
### Fedora and derivatives
```bash
yum install davix
```

### Debian and derivatives
```bash
apt-get install davix
```

### MacOSX
```bash
brew install davix
```

Or you may also install from our own [tap](https://github.com/cern-it-sdc-id/homebrew-dmc).
