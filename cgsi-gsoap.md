# CGSI-gSOAP
Client and server side library to secure gSOAP using the Globus Security
Infrastructure.

It was formerly used by FTS3 SOAP API, and it is still used by DPM SRM server.
