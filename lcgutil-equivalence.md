# lcg-util to gfal2-util

gfal1 and lcg-util have been both deprecated since the *31st October 2014*.
If you are still relying on lcg-util, you will find here a table with the
equivalent commands for gfal2-util.

| Description | lcg-util | gfal2-util |
|-------------|----------|------------|
| Add an alias for a given GUID       | `lcg-aa`           | Deprecated |
| Bring SURLs online                  | `lcg-bringonline`  | `gfal-legacy-bringonline` |
| Copy files with no catalog involved | `lcg-cp`           | `gfal-copy` |
| Copy and register a file            | `lcg-cr`           | `gfal-copy` |
| Delete a file / directory           | `lcg-del`          | `gfal-rm` |
| Get the file checksum               | `lcg-get-checksum` | `gfal-sum` |
| Get the TURLs for given SURLs       | `lcg-getturls`     | `gfal-xattr "user.replicas"` |
| Get the TURL for a given SURL       | `lcg-gt`           | `gfal-xattr "user.replicas"` |
| List aliases for a given LFN/GUID   | `lcg-la`           | Deprecated |
| Get the GUID for a given LFN        | `lcg-lg`           | `gfal-xattr "user.guid"` |
| Lists the replicas for a given LFN  | `lcg-lr`           | `gfal-xattr "user.replicas"` |
| List information / directory        | `lcg-ls`           | `gfal-ls` |
| Remove an alias                     | `lcg-ra`           | Deprecated |
| Copy between Ses using the catalog  | `lcg-rep`          | `gfal-copy` |
| Register a file in a catalog        | `lcg-rf`           | `gfal-copy`  |
| Set a file status to done           | `lcg-sd`           | Deprecated\* |
| Get space tokens associated to a description | `lcg-stmd` | `gfal-xattr "spacetoken"/"spacetoken.token?$tok"/"spacetoken.description?$desc"` |
| Unregister a file                   | `lcg-uf`           | Deprecated |
| Create a directory                  |                  | `gfal-mkdir` |
| Dump the standard input into a file |                  | `gfal-save` |
| Dump a file into the standard output |                 | `gfal-cat` |

(\*) These actions are actually performed internally by the SRM plugin when needed.
